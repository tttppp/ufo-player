#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from mss import mss
from pymouse import PyMouse
import time
from PIL import Image
import random
import shutil

# AI for Tunnel game.
#
# This script should be run so that clicking at CLICK_LOCATION will bring the
# game to the foreground and allow the script to play it.
# Game is at https://andybalaam.gitlab.io/tunnel/

# These values should be adjusted for the size and position of the game.
CLICK_LOCATION = (450, 500)
SCREENSHOT_X_START = 490
SCREENSHOT_X_END = 910
UFO_X = 516 - SCREENSHOT_X_START
Y_MIN = 110
Y_MAX = 1060
Y_STEP_UFO = 1
Y_STEP_TUNNEL = 10
TUNNEL_X = 600 - SCREENSHOT_X_START
TUNNEL_X2 = 700 - SCREENSHOT_X_START
TUNNEL_X3 = 900 - SCREENSHOT_X_START

# Files to use to persist state between executions of the script.
FILE_1 = 'ufoQ.txt'
FILE_BESTS = ['ufoQ0.txt']
DATA_OUTPUT = 'data.csv'
# A 'high score' in terms of the number of decision points hit by a run of the script.
KNOWN_BEST_SCORE = 2000

# The number of decision points to wait to calculate the direction of the UFO.
MAX_HEIGHT_AGE = 5

MINS = (0, -5, -5, -5, -5, 0, -1, 0)
MAXS = (0, 5, 5, 5, 5, 1, 1, 500)
SCALE = (1, 1, 1, 1, 1, 1, 100, 1)

Q = {}

def writeQ(fileName):
    f = open(fileName, 'w')
    for qItem in Q.items():
        f.write(str(qItem) + '\n')
    f.close()

def loadQ(fileName):
    global Q
    
    Q = {}
    f = open(fileName)
    for line in f.readlines():
        line, value = line.strip().strip('(').strip(')').rsplit(', ', 1)
        value = float(value)
        line, action = line.strip().strip('(').strip(')').rsplit(', ', 1)
        action = int(action)
        state = tuple(map(int, line.strip().strip('(').strip(')').split(', ')))
        if abs(state[2] > 1):
            if state[2] > 1:
                positive = (action == 0)
            else:
                positive = (action == 1)
            if positive:
                value = abs(value)
            else:
                value = -abs(value)
        Q[(state, action)] = value
    f.close()

def getBestAction(state):
    bestAction = 0
    #isNew = (state, 0) not in Q.keys()
    if (state, 0) not in Q.keys():
        # Find closest state.
        closest = None
        closestDist = 1000000
        for qState, qAction in Q.keys():
            if qAction != 0:
                continue
            dist = 0
            for i in range(len(state)):
                dist += abs(state[i] - qState[i])
            if dist < closestDist:
                closestDist = dist
                closest = qState
        Q[(state, 0)] = Q[(closest, 0)]
        Q[(state, 1)] = Q[(closest, 1)]
#        if state[-2] > 0:
#            Q[(state, 0)] = random.randrange(2) * 2 - 1
#            Q[(state, 1)] = -Q[(state, 0)]
#        else:
#        if state[2] > 0:
#            Q[(state, 0)] = 1
#            Q[(state, 1)] = -1
#        elif state[2] < 0:
#            Q[(state, 0)] = -1
#            Q[(state, 1)] = 1
#        elif state[4] < 0:
#            Q[(state, 0)] = 1
#            Q[(state, 1)] = -1
#        elif state[4] > 0:
#            Q[(state, 0)] = -1
#            Q[(state, 1)] = 1
#        else:
#            Q[(state, state[-2])] = -1
#            Q[(state, 1-state[-2])] = 1
    bestValue = Q[(state, 0)]
    for a in [0, 1]:
        if Q[(state, a)] > bestValue:
            bestValue = Q[(state, a)]
            bestAction = a
    return bestAction

def outputData(recentStateActionPairs):
    seenSet = set()
    f = open(DATA_OUTPUT, 'a')
    for i, recentPair in enumerate(list(reversed(recentStateActionPairs))):
        if recentPair[0][5] == 1:
            # UFO not present, so not useful for learning.
            continue
        if recentPair in seenSet:
            continue
        seenSet.add(recentPair)
        stateStr = ','.join(map(str, recentPair[0]))
        bestAction = (1 - recentPair[1] if len(seenSet) < 10 else recentPair[1])
        f.write('{},{}\n'.format(stateStr, bestAction))
    f.close()

def updateQ(recentStateActionPairs):
    finalUFOY = None
    seenSet = set()
    changedSet = set()
    notPresentCount = 0
    for i, recentPair in enumerate(list(reversed(recentStateActionPairs))):
        if recentPair[0][5] == 1:
            # UFO not present, so not useful for learning.
            notPresentCount += 1
            continue
        if finalUFOY == None:
            finalUFOY = recentPair[0][1]
        if recentPair in seenSet:
            continue
        #print(finalUFOY, len(seenSet), recentPair)
        seenSet.add(recentPair)
        if random.randrange(i+2-len(seenSet) - notPresentCount) > 0:
            continue
        makeChange = False
        # Try changing some recent decision points at random.
        if random.randrange(10) == 0:
            makeChange = True
            hit = 'Not sure'
        otherPair = (recentPair[0], 1-recentPair[1])
        if finalUFOY > 0 and recentPair[1] == 1:
            makeChange = True
            hit = 'Ceiling'
        if finalUFOY < 0 and recentPair[1] == 0:
            makeChange = True
            hit = 'Floor'
        if finalUFOY == 0:
            makeChange = True
            hit = 'Unknown'
        if makeChange:
            oldQ = (0 if recentPair not in Q.values() else Q[recentPair])
            Q[recentPair] = -1
            Q[otherPair] = 1
            print(finalUFOY, i, recentPair, oldQ, Q[recentPair], hit)
            changedSet.add(recentPair[0])
            alsoChangeRange = random.randrange(3) + 1
            state = recentPair[0]
            for qState, qAction in Q.keys():
                if qAction != recentPair[1]:
                    continue
                # Don't change other phases.
                if qState[-1] != state[-1]:
                    continue
                dist = 0
                for i in range(len(state)):
                    dist += abs(state[i] - qState[i])
                if dist < alsoChangeRange and random.randrange(2) == 0:
                    Q[(qState, qAction)] = -1
                    Q[(qState, 1-qAction)] = 1
                    print('Also changed', qState, qAction, dist)
            if random.randrange(len(changedSet)) > 5:
                break

def applyStateLimits(state):
    result = []
    for i, s in enumerate(state):
        if s < MINS[i]:
            s = MINS[i]
        elif s > MAXS[i]:
            s = MAXS[i]
        s *= SCALE[i]
        result.append(s)
    return tuple(result)

def makeCoordinateSlice(x, yStep):
    coords = []
    for y in range(Y_MIN, Y_MAX, yStep):
        coords.append((x, y))
    return coords

def screenGrab():
    with mss() as sct:
        # Get rid of the first, as it represents the "All in One" monitor:
        monitor = {"top": 0, "left": SCREENSHOT_X_START, "width": SCREENSHOT_X_END - SCREENSHOT_X_START, "height": Y_MAX}
        sct_img = sct.grab(monitor)
        return sct_img

def getTunnelCoords(rgb_im, tunnelX, yStepTunnel):
    top, bottom = None, None
    for c in makeCoordinateSlice(tunnelX, yStepTunnel):
        r, g, b = rgb_im.getpixel(c)
        if (r == 0 and g == 0 and b == 0):
            top = c[1]
            break
    for c in reversed(makeCoordinateSlice(tunnelX, yStepTunnel)):
        r, g, b = rgb_im.getpixel(c)
        if (r == 0 and g == 0 and b == 0):
            bottom = c[1]
            break
    return top, bottom

def getAvgTunnelCoords(rgb_im, tunnelX, yStepTunnel):
    increment = 4
    tops = []
    bottoms = []
    for xx in range(1):
        top, bottom = getTunnelCoords(rgb_im, tunnelX + xx * increment, yStepTunnel)
        tops.append(top)
        bottoms.append(bottom)
    return sum(tops) / len(tops), sum(bottoms) / len(bottoms)

def getUFOHeight(rgb_im, lastHeight):
    for c in reversed(makeCoordinateSlice(UFO_X, Y_STEP_UFO)):
        try:
            r, g, b = rgb_im.getpixel(c)
        except:
            print('Issue with coord', c)
            raise
        #if (r == 212 and g == 212 and b == 212) or (r == 0 and g == 240 and b == 66) or (r == 243 and g == 27 and b == 244) or (r == 249 and g == 120 and b == 91):
        if (r == 0 and g == 240 and b == 66) or (r == 243 and g == 27 and b == 244) or (r == 249 and g == 120 and b == 91):
            return c[1]
    # Guess no change
    return None

def main():
    lastHeight = [500] * MAX_HEIGHT_AGE
    # Map from fileId to best score
    bestScores = {}
    # Map from fileId to the state-action pairs
    stateActionPairs = {}
    m = PyMouse()
    lastScore = 51
    for ii in range(5000):
        fileId = random.randrange(len(FILE_BESTS))
        loadQ(FILE_BESTS[fileId])
        if fileId in stateActionPairs.keys():
            # Make edits
            updateQ(stateActionPairs[fileId])
            writeQ(FILE_BESTS[fileId])
        
        m.click(*CLICK_LOCATION)
        time.sleep(0.5)
        m.click(*CLICK_LOCATION)
        
        unseenFor = 0
        pressing = 0
        timeSincePressChange = 0
        recentStateActionPairs = []
        recentTops = [[], [], []]
        recentBottoms = [[], [], []]

        for i in range(100000):
            sct_img = screenGrab()
            
            im = Image.frombytes("RGB", sct_img.size, sct_img.bgra, "raw", "BGRX")
            rgb_im = im.convert('RGB')
            
            top, bottom = getAvgTunnelCoords(rgb_im, TUNNEL_X, Y_STEP_TUNNEL)
            top2, bottom2 = getAvgTunnelCoords(rgb_im, TUNNEL_X2, Y_STEP_TUNNEL)
            top3, bottom3 = getAvgTunnelCoords(rgb_im, TUNNEL_X3, Y_STEP_TUNNEL)
            tunnelHeight = abs(bottom2 - top2)
            
            if i % 10 == 0:
                recentTops[0].append(top)
                recentBottoms[0].append(bottom)
                recentTops[1].append(top2)
                recentBottoms[1].append(bottom2)
                recentTops[2].append(top3)
                recentBottoms[2].append(bottom3)
            
                for topBottomIndex in range(len(recentTops)):
                    if len(recentTops[topBottomIndex]) > 5:
                        recentTops[topBottomIndex].pop(0)
                        recentBottoms[topBottomIndex].pop(0)
            else:
                recentTops[0][-1] = min(top, recentTops[0][-1])
                recentBottoms[0][-1] = max(bottom, recentBottoms[0][-1])
                recentTops[1][-1] = min(top2, recentTops[1][-1])
                recentBottoms[1][-1] = max(bottom2, recentBottoms[1][-1])
                recentTops[2][-1] = min(top3, recentTops[2][-1])
                recentBottoms[2][-1] = max(bottom3, recentBottoms[2][-1])
            
            top, bottom = min(recentTops[0]), max(recentBottoms[0])
            top2, bottom2 = min(recentTops[1]), max(recentBottoms[1])
            top3, bottom3 = min(recentTops[2]), max(recentBottoms[2])
            
            # The colours and heights used by each phase.
            #red = (0xff, 0x00, 0x21) # 600
            #green = (0x00, 0xc9, 0x34) # 450
            #blue = (0x00, 0x11, 0xf7) # 400
            #purple = (0xff, 0x00, 0xf8) # 333
            #orange = (0xff, 0x7d, 0x2f) # 300
            heights = [600, 450, 400, 333, 300]
            # Guess the phase from the tunnel height.
            nearest = 0
            bestDiff = 600
            for phase, h in enumerate(heights):
                diff = abs(tunnelHeight - h)
                if diff < bestDiff:
                    bestDiff = diff
                    nearest = phase
            phase = nearest

            ufo = getUFOHeight(rgb_im, lastHeight[0])
            
            im.close()

            # Check if user wants to kill program by movig the mouse.
            if ufo == None:
                if i > 50 and unseenFor > 10:
                    if fileId not in stateActionPairs.keys():
                        bestScores[fileId] = i
                    stateActionPairs[fileId] = recentStateActionPairs
                    print('Attempt:', ii, 'Id:', fileId, 'Score:', i, 'Files:', bestScores, 'Best:', i > bestScores[fileId])
                    if i >= lastScore:
                        print('Persisting to ', FILE_BESTS[fileId])
                        writeQ(FILE_BESTS[fileId])
                        outputData(recentStateActionPairs)
                        if i >= max(bestScores.values()) and i > KNOWN_BEST_SCORE:
                            print('BEST YET')
                            shutil.copy(FILE_BESTS[fileId], FILE_1)
                        bestScores[fileId] = i
                    if i > 100:
                        lastScore = i
                    print('Best:', bestScores[fileId])

                    m.release(*CLICK_LOCATION)
                    m.click(*CLICK_LOCATION)
                    time.sleep(1)
                    m.click(*CLICK_LOCATION)
                    break
                unseenFor += 1
                ufo = lastHeight[0]
            else:
                unseenFor = 0
            # Apply model to get boost duration.
            target = ((top + bottom) / 2 - ufo)
            target2 = ((top2 + bottom2) / 2 - ufo)
            target3 = ((top3 + bottom3) / 2 - ufo)
            direction = (ufo - lastHeight[MAX_HEIGHT_AGE - 1])
            
            if i > 20:
                currentPos = m.position()
                if abs(CLICK_LOCATION[0] - currentPos[0]) + abs(CLICK_LOCATION[1] - currentPos[1]) > 1:
                    print('Ending due to mouse movement')
                    return
            
            state = (i, int(15 * target / tunnelHeight), int(15 * target2 / tunnelHeight), int(15 * target3 / tunnelHeight), int(direction), 1 if unseenFor > 0 else 0, pressing, phase)
            state = applyStateLimits(state)
            action = getBestAction(state)
            timeSincePressChange = 0
            recentStateActionPairs.append((state, action))
            
            if action == 1:
                m.press(*CLICK_LOCATION)
                if pressing == 1:
                    timeSincePressChange += 1
                else:
                    timeSincePressChange = 0
                pressing = 1
            else:
                m.release(*CLICK_LOCATION)
                if pressing == 0:
                    timeSincePressChange += 1
                else:
                    timeSincePressChange = 0
                pressing = 0

            # Store the last known height
            for t in range(MAX_HEIGHT_AGE - 1, 0, -1):
                lastHeight[t] = lastHeight[t - 1]
            lastHeight[0] = ufo
 
if __name__ == '__main__':
    main()
