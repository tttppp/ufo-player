# An AI player for the Tunnel game

Try the game out here: https://gitlab.com/andybalaam/tunnel

[Watch a demo of the AI player here](https://vimeo.com/382290898)

The AI player is written in Python and uses MSS to grab screenshots of the game, and PyMouse to click and
release the mouse button.  In theory it learns to play better over time, but I'm not convinced this works
very well.

If you want to try the AI yourself then you will need to update the constant values near the top of the
file to match your own screen layout.

## Previous approaches

### Boost duration

My first version used a linear combination of features and an activation function to determine how long to
fire the boosters for at a time. It made small changes to the linear coefficients and stored these if they
caused an improvement to the score.

The features I picked were:
* The percentage distance of the UFO from the middle of the tunnel at three X coordinates.
* The verical velocity of the UFO.
* The length of time the UFO hasn't been seen for (because the UFO is not present between games).
* The duration since the last mouse button click/release.
* The phase (determined from the tunnel height).

### Q-learning

The second version used Q-learning and was inspired by a couple of articals about writing Flappy Bird AIs:
* [Flappy Bird RL by sarvagyavaish](http://sarvagyavaish.github.io/FlappyBirdRL/)
* [Flappy Bird Bot using Reinforcement Learning in Python by chncyhn](https://github.com/chncyhn/flappybird-qlearning-bot)

I tried lots of different variants of this, and it was certainly able to learn from mistakes, but
ultimately I couldn't find a good balance between convergence and performance.  By having a large state
space size I was able to get an initially good score (e.g. by initialising Q to boost when the UFO was
below the middle of the tunnel), but after a while the AI wanted to explore the other options, and then
got stuck while finding the thousands of different ways to crash into the walls. On the other hand,
having a small state space meant that the AI never managed to get past certain parts of the tunnel.

### Decision table

Simplifying the Q-learning approach to simply storing the decisions and making small changes to the ones
that resulted in a crash gave a pretty good and consistent score.  If the UFO crashes into the ceiling
then the AI tries to find a recent state where it boosted and updates it so that it won't boost next time.

The [demo video](https://vimeo.com/382290898) shows the performance of this, and it usually reaches scores
of about 9000-10000.

## Potential improvements

There is lots of room for improvement, but here are a few things that I think would help:

### Pay attention to the bonuses.

Currently the AI ignores these, but picks some up by accident.

### Use a faster language/faster hardware.

Currently decisions are made as fast and as frequently as possible, but taking screenshots is expensive
and Python is slow at dealing with the large object.  I noticed a massive improvement when I started only
taking screenshots of a region of the screen.

### Store all the decisions and results.

By storing all the decisions and results (i.e. crash within a second/no crash) then it would be possible
to train an algorithm offline.  Given how well the "boost when below the middle of the tunnel" strategy
works then it seems very likely that a very good AI can be created using a very simple strategy.
Currently the fact that the AI interacts with the game periodically means that small differences in
timings can result in very different behaviours.